#include <stdio.h>
#include <stdlib.h>
#include "database.h"
#include <string.h>

int main(){

    Database * d=NULL;

    Persona* p1=(Persona*)malloc(sizeof(Persona));
    Persona* p2=(Persona*)malloc(sizeof(Persona));
    Persona* p3=(Persona*)malloc(sizeof(Persona));
    Persona* p4=(Persona*)malloc(sizeof(Persona));
    Persona* pFind=(Persona*)malloc(sizeof(Persona));
    Persona* pFind1=(Persona*)malloc(sizeof(Persona));
    Persona* pFind2=(Persona*)malloc(sizeof(Persona));
    Persona* pFind3=(Persona*)malloc(sizeof(Persona));

    strcpy(p1->name,"Andrea");
    strcpy(p1->surname,"Pulsoni");
    strcpy(p1->address,"Ciao");
    p1->age=21;

    strcpy(p2->name,"Angela");
    strcpy(p2->surname,"Bianchi");
    strcpy(p2->address,"Casa");
    p2->age=22;

    strcpy(p3->name,"Pippo");
    strcpy(p3->surname,"Topo");
    strcpy(p3->address,"Hotel");
    p3->age=82;

    strcpy(p4->name,"Aladin");
    strcpy(p4->surname,"Genio");
    strcpy(p4->address,"Monte");
    p4->age=2;    

    d=insert(d,p1);
    d=insert(d,p2);
    d=insert(d,p3);
    d=insert(d,p4);

    print_tree_str(d->name);
    print_tree_str(d->surname);
    print_tree_str(d->address);
    print_tree_int(d->age);

    printf("___Find by name___\n");
    pFind=findByName(d,"Aladin");
    printf("___Find by surname___\n");
    pFind1=findBySurname(d,"Silia");
    printf("___Find by address___\n");
    pFind2=findByAddress(d,"Hotel");
    printf("___Find by age___\n");
    pFind3=findByAge(d,21);

    return 0;
}