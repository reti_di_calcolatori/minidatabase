#include "database.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

IndexNodeString* create_node_string(char* n){
    IndexNodeString* nString=(IndexNodeString*)malloc(sizeof(IndexNodeString));
    if(nString==NULL)return NULL;
    nString->value=n;
    nString->right=NULL;
    nString->left=NULL;
    return nString;
}

IndexNodeInt* create_node_int(int a){
    IndexNodeInt* nInt=(IndexNodeInt*)malloc(sizeof(IndexNodeInt));
    nInt->value=a;
    nInt->right=NULL;
    nInt->left=NULL;
    return nInt;
}

Database* initialize_database(char* name, char* surname, char* address, int age){
    Database* d=(Database*)malloc(sizeof(Database));
    d->name=create_node_string(name);
    d->surname=create_node_string(surname);
    d->address=create_node_string(address);
    d->age=create_node_int(age);
    return d;
}

int find_min_path(IndexNodeString* t){
    int lf=0;
    int rg=0;
    if(t==NULL){
        return 0;
    } else {
        lf =find_min_path(t->left)+1;
        rg =find_min_path(t->right)+1;
    }
    if(lf<rg) return lf;
    else return rg;
}

IndexNodeString* insert_string(IndexNodeString* name, char* n, int pos, int* check){
    if(pos==0 && name==NULL){
        name=create_node_string(n);
        *check=0;
        return name;
    } else if(pos==0 && name!=NULL){
        return name;
    }else{
        name->left=insert_string(name->left,n,pos-1,check);
        if(*check!=0){
            name->right=insert_string(name->right,n,pos-1,check);
        }
        
        return name;
    }
}

IndexNodeInt* insert_int(IndexNodeInt* age, int a, int pos, int*check){
    if(pos==0 && age==NULL){
        age=create_node_int(a);
        *check=0;
        return age;
    } else if(pos==0 && age!=NULL){
        return age;
    }else{
        
        age->left=insert_int(age->left,a,pos-1,check);
        if(*check!=0){
            age->right=insert_int(age->right,a,pos-1,check);
        }
        
        return age;
    }
}

Database* insert(Database * database, Persona * persona){
    if(database==NULL){
        database=initialize_database(persona->name,persona->surname,persona->address,persona->age);
        return database;
    }
    int path=find_min_path(database->name);
    int check=1;
    database->name=insert_string(database->name,persona->name,path,&check);
    check=1;
    database->surname=insert_string(database->surname,persona->surname,path,&check);
    check=1;
    database->address=insert_string(database->address,persona->address,path,&check);
    check=1;
    
    database->age=insert_int(database->age,persona->age,path,&check);
    return database;
    
}

void print_tree_str(IndexNodeString * root) {
    if (root == NULL) {
        return;
    }
    print_tree_str(root->left);
    printf("%s\n", root->value);
    print_tree_str(root->right);
}

void print_tree_int(IndexNodeInt * root) {
    if (root == NULL) {
        return;
    }
    print_tree_int(root->left);
    printf("%d\n", root->value);
    print_tree_int(root->right);
}

Persona* findByName_aux(IndexNodeString* name, IndexNodeString* surname, IndexNodeString* address, IndexNodeInt* age, char* n){
    Persona* p=NULL;
    if(name==NULL){
        return NULL;
    } else if(strcmp(name->value,n)==0) {
        p=(Persona*)malloc(sizeof(Persona));
        strcpy(p->name,name->value);
        strcpy(p->surname,surname->value);
        strcpy(p->address,address->value);
        p->age=age->value;
        return p;
    } else {
        p=findByName_aux(name->left,surname->left,address->left,age->left,n);
        if(p==NULL){
            p=findByName_aux(name->right,surname->right,address->right,age->right,n);
        }
    }
    return p;
}

Persona* findByName(Database * database, char * name){
    Persona* p=(Persona*)malloc(sizeof(Persona));
    p=findByName_aux(database->name,database->surname,database->address,database->age,name);
    
    if(p==NULL){
        printf("Person not found\n");
    } else{
        printf("Person found\n");
        printf("Name: %s\n",p->name);
        printf("Surname: %s\n",p->surname);
        printf("Address: %s\n",p->address);
        printf("Age: %d\n",p->age);
    }
    return p;
    
}

Persona* findBySurname_aux(IndexNodeString* name, IndexNodeString* surname, IndexNodeString* address, IndexNodeInt* age, char* n){
    Persona* p=NULL;
    if(surname==NULL){
        return NULL;
    } else if(strcmp(surname->value,n)==0) {
        p=(Persona*)malloc(sizeof(Persona));
        strcpy(p->name,name->value);
        strcpy(p->surname,surname->value);
        strcpy(p->address,address->value);
        p->age=age->value;
        return p;
    } else {
        p=findBySurname_aux(name->left,surname->left,address->left,age->left,n);
        if(p==NULL){
            p=findBySurname_aux(name->right,surname->right,address->right,age->right,n);
        }
    }
    return p;
}

Persona* findBySurname(Database * database, char * surname){
    Persona* p=(Persona*)malloc(sizeof(Persona));
    p=findBySurname_aux(database->name,database->surname,database->address,database->age,surname);
    
    if(p==NULL){
        printf("Person not found\n");
    } else{
        printf("Person found\n");
        printf("Name: %s\n",p->name);
        printf("Surname: %s\n",p->surname);
        printf("Address: %s\n",p->address);
        printf("Age: %d\n",p->age);
    }
    return p;
}

Persona* findByAddress_aux(IndexNodeString* name, IndexNodeString* surname, IndexNodeString* address, IndexNodeInt* age, char* n){
    Persona* p=NULL;
    if(address==NULL){
        return NULL;
    } else if(strcmp(address->value,n)==0) {
        p=(Persona*)malloc(sizeof(Persona));
        strcpy(p->name,name->value);
        strcpy(p->surname,surname->value);
        strcpy(p->address,address->value);
        p->age=age->value;
        return p;
    } else {
        p=findByAddress_aux(name->left,surname->left,address->left,age->left,n);
        if(p==NULL){
            p=findByAddress_aux(name->right,surname->right,address->right,age->right,n);
        }
    }
    return p;
}

Persona* findByAddress(Database * database, char * address){
    Persona* p=(Persona*)malloc(sizeof(Persona));
    p=findByAddress_aux(database->name,database->surname,database->address,database->age,address);
    
    if(p==NULL){
        printf("Person not found\n");
    } else{
        printf("Person found\n");
        printf("Name: %s\n",p->name);
        printf("Surname: %s\n",p->surname);
        printf("Address: %s\n",p->address);
        printf("Age: %d\n",p->age);
    }
    return p;
}

Persona* findByAge_aux(IndexNodeString* name, IndexNodeString* surname, IndexNodeString* address, IndexNodeInt* age, int n){
    Persona* p=NULL;
    if(age==NULL){
        return NULL;
    } else if(age->value==n) {
        p=(Persona*)malloc(sizeof(Persona));
        strcpy(p->name,name->value);
        strcpy(p->surname,surname->value);
        strcpy(p->address,address->value);
        p->age=age->value;
        return p;
    } else {
        p=findByAge_aux(name->left,surname->left,address->left,age->left,n);
        if(p==NULL){
            p=findByAge_aux(name->right,surname->right,address->right,age->right,n);
        }
    }
    return p;
}

Persona* findByAge(Database * database, int age){
    Persona* p=(Persona*)malloc(sizeof(Persona));
    p=findByAge_aux(database->name,database->surname,database->address,database->age,age);
    
    if(p==NULL){
        printf("Person not found\n");
    } else{
        printf("Person found\n");
        printf("Name: %s\n",p->name);
        printf("Surname: %s\n",p->surname);
        printf("Address: %s\n",p->address);
        printf("Age: %d\n",p->age);
    }
    return p;
}

